export const state = () => ({
    lightmode: false
})


export const actions = {
    toggleLightmode({commit}, lightmode) {
        commit('TOGGLE_LIGHTMODE', lightmode)
        document.body.classList.toggle("light-mode")
    }
}

export const mutations = {
    TOGGLE_LIGHTMODE(state) {
        // state.lightmode == false ? true : false
        state.lightmode = !state.lightmode
        // console.log(state.lightmode);
    }
}
