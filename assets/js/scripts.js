
// TILTS
var tilts = document.querySelectorAll('.tilt');
VanillaTilt.init(tilts, {
    max: 7,
    speed: 10000,
    transition: true,
    easing: "cubic-bezier(.03,.98,.52,.99)"
});