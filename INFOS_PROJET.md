


### Modif vue-chartjs :

Modification apporté au module afin de le rendre compatible avec chartjs ^3.0.0v.   
Dans le fichier node_modules/vue-chartjs/es/BaseCharts.js :  
```js
--- import Chart from 'chart.js';
+++ import Chart from 'chart.js/auto';
```

source: https://github.com/apertureless/vue-chartjs/issues/695