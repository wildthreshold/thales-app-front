export default {
	// server: {
	//   port: 8000, // default: 3000
	// },

	// Target: https://go.nuxtjs.dev/config-target
	target: "server",

	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: "Cyberthreat Hitmap",
		htmlAttrs: {
			lang: "en",
		},
		meta: [{
				charset: "utf-8"
			},
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1"
			},
			{
				hid: "description",
				name: "description",
				content: ""
			},
			{
				name: "format-detection",
				content: "telephone=no"
			},
		],
		link: [{
			rel: "icon",
			type: "image/x-icon",
			href: "/favicon.ico"
		}],
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: [
		"~/assets/scss/_grid.scss",
		"~/assets/scss/global.scss",
		"~/assets/scss/forms.scss",
		"~/assets/scss/helpers.scss",
		"~/assets/scss/lightmode.scss",
		"~/assets/scss/areas.scss",
		"~/assets/scss/media-library.scss",
	],
	// Global js: https://go.nuxtjs.dev/config-css
	js: ["~/assets/js/gsap.js"],

	// Plugins to run before rendering page:
	// https://go.nuxtjs.dev/config-plugins
	plugins: [
		'~/plugins/utils.js',
	],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended):
	// https://go.nuxtjs.dev/config-modules
	buildModules: [
		"@nuxtjs/dotenv",
		'@nuxtjs/moment',
	],

	// moment: {
	// 	locales: ['fr']
	// },

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://go.nuxtjs.dev/axios
		"@nuxtjs/axios",
		'@nuxtjs/gtm',
		// 'nuxt-cookie-control',
		// ['nuxt-cookie-control', {
		// 	text: {
		// 		barTitle: '',
		// 		barDescription: 'We use our own cookies and third-party cookies so that we can show you this website and better understand how you use it, with a view to improving the services we offer. If you continue browsing, we consider that you have accepted the cookies.',
		// 		acceptAll: 'Accept all',
		// 		declineAll: 'Delete all',
		// 		manageCookies: 'Manage',
		// 		unsaved: 'You have unsaved settings',
		// 		close: 'Close',
		// 		save: 'Save',
		// 		necessary: 'Necessary cookies',
		// 		optional: 'Optional cookies',
		// 		functional: 'Functional cookies',
		// 		blockedIframe: 'To see this, please enable functional cookies',
		// 		here: 'here'
		// 	}
		// }]
	],

	// cookies: {
	// 	optional: [
	// 		{
	// 			name:  'Google Analitycs',
	// 			//if you don't set identifier, slugified name will be used
	// 			identifier: 'ga',
	// 			//if multilanguage
	// 			// description: {
	// 			// 	en:  'Google GTM is ...'
	// 			// },
	// 			//else
	// 			description:  '',
		
	// 			initialState: true,
	// 			src:  'https://www.googletagmanager.com/gtag/js?id=GTM-P3V3GVF',
	// 			async:  true,
	// 			// cookies: ['_ga'],
	// 			// accepted: () => {
	// 			// 	window.dataLayer = window.dataLayer || [];
	// 			// 	window.dataLayer.push({
	// 			// 		'gtm.start': new Date().getTime(),
	// 			// 		event: 'gtm.js'
	// 			// 	});
	// 			// },
	// 			// declined: () =>{}
	// 		}
	// 	]
	// },

	gtm: {
		id: 'GTM-P3V3GVF',
		// autoInit: false,
		enabled: true
	},

	// Axios module configuration: https://go.nuxtjs.dev/config-axios
	axios: {},

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {
		transpile: ['gsap'],
		extend(config, ctx) {
			if (ctx.isDev) {
				config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
			}
		},
		loaders: {
			// sass: { implementation: require("sass") },
			scss: {
				implementation: require("sass")
			},
		},
	},
};
