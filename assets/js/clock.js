// HORLOGE ET DATE DU JOUR
const renderHour = function () {
	const time = new Date();
	// clock.textContent = "";
    time.toLocaleString('fr-FR', { hour: 'numeric', minute: 'numeric', second: 'numeric'});
};
const renderDate = function () {
	const dateToday = new Date();
	// datediv.textContent = 
    dateToday.toLocaleString('fr-FR', { year: 'numeric', month: 'long', day: 'numeric'});
};
renderHour();
renderDate();
// Update time chaque second
setInterval(renderHour, 1000);